import { Component } from '@angular/core';
import { DataSet } from '@antv/data-set';
import { HttpClient } from '@angular/common/http';

const nodesLabel = [
  'name', {
    offset: 0,
    textStyle: (text, item) => {
      if (item.point.hasChildren) {
        return {
          opacity: 0
        };
        // return {
        //   textBaseline: 'middle',
        //   fill: 'red',
        //   fontSize: 15,
        //   textAlign: 'center'
        // };
      }
      return {
        textBaseline: 'middle',
        fill: 'grey',
        fontSize: 9,
        textAlign: 'center'
      };
    }
  },
];

// const nodesLabel = [
//   'name', {
//     offset: 0,
//     labelEmit: true,
//     textStyle: (text, item) => {
//       let textAlign = item.textAlign;
//       if (item.point.hasChildren) {
//         textAlign = textAlign === 'left' ? 'right' : 'left';
//       }
//       return {
//         fill: 'grey',
//         fontSize: 15,
//         textAlign,
//       };
//     },
//   },
// ];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})


export class AppComponent {
  height = 100;
  width = 100;
  padding = [0, 0, 0, 0];
  data = {};
  size = ['r', r => r * 100];
  style = { stroke: 'rgb(183, 55, 121)' };
  color = ['r', 'rgb(252, 253, 191)-rgb(231, 82, 99)-rgb(183, 55, 121)'];
  label = nodesLabel;

  constructor(private http: HttpClient) {
    this.http.get('./assets/1.json').subscribe((data) => {
      console.log(data);

      const dv = new DataSet.View().source(data, {
        type: 'hierarchy',
      });

      dv.transform({
        type: 'hierarchy.circle-packing',
      });

      console.log(dv.getAllNodes());

      this.data = dv.getAllNodes().map(node => ({
        hasChildren: !!(node.data.children && node.data.children.length),
        name: node.data.name.split(/(?=[A-Z][^A-Z])/g).join('\n'),
        value: node.value,
        depth: node.depth,
        x: node.x,
        y: node.y,
        r: node.r
      }));

      console.log(this.data);

    });
  }

  /***
   * Gráfico 2
   */

  // forceFit = true;
  // height = 800;
  // padding = [60, 0, 40, 0];
  // edgeSource = {};
  // nodeSource = {};
  // label = nodesLabel;

  // constructor(private http: HttpClient) {

  //   this.http.get('./assets/1.json').subscribe((sourceData) => {

  //     const dv = new DataSet.View().source(sourceData, {
  //       type: 'hierarchy',
  //     });
  //     dv.transform({
  //       type: 'hierarchy.tree',
  //     });

  //     this.edgeSource = dv.getAllLinks().map(link => ({
  //       x: [link.source.x, link.target.x],
  //       y: [link.source.y, link.target.y],
  //       source: link.source.id,
  //       target: link.target.id
  //     }));

  //     this.nodeSource = dv.getAllNodes().map(node => ({
  //       hasChildren: !!(node.data.children && node.data.children.length),
  //       name: node.data.name,
  //       value: node.value,
  //       depth: node.depth,
  //       x: node.x,
  //       y: node.y
  //     }));
  //   });
  // }

}
